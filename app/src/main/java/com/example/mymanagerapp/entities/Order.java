package com.example.mymanagerapp.entities;

public class Order {
    private int id;
    private String address;
    private String order;
    private int userId;

    public Order(int id, String address, String order, int userId) {
        this.id = id;
        this.address = address;
        this.order = order;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getOrder() {
        return order;
    }

    public int getUserId() {
        return userId;
    }
}
