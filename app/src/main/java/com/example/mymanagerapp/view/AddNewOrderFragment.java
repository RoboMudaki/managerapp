package com.example.mymanagerapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mymanagerapp.R;
import com.example.mymanagerapp.controller.ControllerAddNewCourier;
import com.example.mymanagerapp.controller.ControllerAddNewOrder;

public class AddNewOrderFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_add_new_order, container, false);

        ControllerAddNewOrder controller = new ControllerAddNewOrder(view);
        controller.InitializeButton();

        return view;
    }
}
