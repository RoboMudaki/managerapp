package com.example.mymanagerapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mymanagerapp.R;
import com.example.mymanagerapp.controller.ControllerLoginPage;

public class LoginPageFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_login_page, container, false);

        ControllerLoginPage controller = new ControllerLoginPage(view);
        controller.InitializeButtonsClick();

        return view;
    }
}
