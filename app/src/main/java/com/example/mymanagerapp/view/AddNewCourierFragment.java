package com.example.mymanagerapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mymanagerapp.R;
import com.example.mymanagerapp.controller.ControllerAddNewCourier;
import com.example.mymanagerapp.controller.ControllerCourierInfo;

public class AddNewCourierFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_add_new_courier, container, false);

        ControllerAddNewCourier controller = new ControllerAddNewCourier(view);
        controller.InitializeButton();

        return view;
    }
}
