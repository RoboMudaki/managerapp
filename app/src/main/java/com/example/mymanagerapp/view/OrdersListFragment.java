package com.example.mymanagerapp.view;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mymanagerapp.R;
import com.example.mymanagerapp.controller.ControllerCouriersList;
import com.example.mymanagerapp.controller.ControllerOrdersList;

public class OrdersListFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_orders_list, container, false);

        ControllerOrdersList controller =new ControllerOrdersList(view);
        controller.InitializeButton();
        controller.ShowAllOrders();

        return view;
    }
}
