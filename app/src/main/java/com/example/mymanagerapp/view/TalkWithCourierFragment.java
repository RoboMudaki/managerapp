package com.example.mymanagerapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mymanagerapp.R;
import com.example.mymanagerapp.controller.ControllerTalkWithCourier;


public class TalkWithCourierFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragmetn_talk_with_ccourier, container, false);

        ControllerTalkWithCourier controller = new ControllerTalkWithCourier(view);
        controller.InitializeButtons();

        return view;
    }
}
