package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.entities.Courier;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbManager;
import com.example.mymanagerapp.view.AddNewCourierFragment;

import java.util.ArrayList;

public class ControllerCouriersList {
    View view;
    DbManager db;


    public ControllerCouriersList(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void InitializeButton(){
        Button buttonAddNewCourier = view.findViewById(R.id.buttonGoToAddindCourierPage);
        buttonAddNewCourier.setOnClickListener(ButtonAddNewCourierOnClick);
    }

    private View.OnClickListener ButtonAddNewCourierOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AddNewCourierFragment addNewCourierFragment = new AddNewCourierFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, addNewCourierFragment);
            fragmentTransaction.commit();
        }
    };

    public void ShowAllOrders(){
        ArrayList<Courier> couriers = db.getTableCouriers().GetCouriers();

        Context context = (Context) DataStorage.Get("context");

        RecyclerView recyclerViewCatalog = view.findViewById(R.id.recyclerViewCouriers);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerViewCatalog.setLayoutManager(llm);

        RvAdapterCourierList adapter = new RvAdapterCourierList(couriers);
        recyclerViewCatalog.setAdapter(adapter);
    }
}
