package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.entities.Courier;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.view.CourierInfoFragment;

import java.util.ArrayList;

public class RvAdapterCourierList extends RecyclerView.Adapter<RvAdapterCourierList.OrderViewHolder> {
    ArrayList<Courier> couriers;

    public RvAdapterCourierList(ArrayList<Courier> orders) {
        this.couriers = orders;
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewCourierId;
        public TextView textViewCourierName;

        public Button buttonTakeGoToCourierInfo;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewCourierId = itemView.findViewById(R.id.textViewCourierId);
            textViewCourierName = itemView.findViewById(R.id.textViewCourierName);
            buttonTakeGoToCourierInfo = itemView.findViewById(R.id.buttonGoToCourierInfo);
        }
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_courier_list_item,viewGroup,false);
        OrderViewHolder ovh = new OrderViewHolder(v);
        return ovh;
    }



    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int i) {
        holder.textViewCourierId.setText("id: "+couriers.get(i).getId());
        holder.textViewCourierName.setText(couriers.get(i).getName()+" "+couriers.get(i).getLastName());

        holder.buttonTakeGoToCourierInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DataStorage.Add("CurrentCourier", couriers.get(i));

                MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

                CourierInfoFragment courierInfoFragment = new CourierInfoFragment();

                FragmentTransaction fragmentTransaction;
                fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragmentsContainerMain, courierInfoFragment);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return couriers.size();
    }
}

