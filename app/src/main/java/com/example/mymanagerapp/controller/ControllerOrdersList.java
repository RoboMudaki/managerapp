package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.entities.Courier;
import com.example.mymanagerapp.entities.Order;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbManager;
import com.example.mymanagerapp.view.AddNewCourierFragment;
import com.example.mymanagerapp.view.AddNewOrderFragment;
import com.example.mymanagerapp.view.CourierInfoFragment;

import java.util.ArrayList;

public class ControllerOrdersList {
    View view;
    DbManager db;


    public ControllerOrdersList(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void InitializeButton(){
        Button buttonAddNewOrder = view.findViewById(R.id.buttonGoToAddingNewOrder);
        buttonAddNewOrder.setOnClickListener(ButtonAddNewOrderOnClick);

        Button buttonBack = view.findViewById(R.id.buttonBackToCourierInfo);
        buttonBack.setOnClickListener(ButtonBackOnClick);
    }

    private View.OnClickListener ButtonBackOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CourierInfoFragment courierInfoFragment = new CourierInfoFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, courierInfoFragment);
            fragmentTransaction.commit();
        }
    };

    private View.OnClickListener ButtonAddNewOrderOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AddNewOrderFragment addNewOrderFragment = new AddNewOrderFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, addNewOrderFragment);
            fragmentTransaction.commit();
        }
    };

    public void ShowAllOrders(){
        Courier currentCourier = (Courier) DataStorage.Get("CurrentCourier");

        ArrayList<Order> orders = db.getTableOrders().GetOrdersByUserId(currentCourier.getId());

        Context context = (Context) DataStorage.Get("context");

        RecyclerView recyclerViewCatalog = view.findViewById(R.id.recyclerViewOrders);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerViewCatalog.setLayoutManager(llm);

        RvAdapterOrdersList adapter = new RvAdapterOrdersList(orders);
        recyclerViewCatalog.setAdapter(adapter);
    }
}
