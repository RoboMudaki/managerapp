package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.entities.Courier;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbManager;
import com.example.mymanagerapp.view.CourierStatsFragment;
import com.example.mymanagerapp.view.CouriersListFragment;
import com.example.mymanagerapp.view.OrdersListFragment;
import com.example.mymanagerapp.view.TalkWithCourierFragment;

public class ControllerCourierInfo {
    private View view;
    private DbManager db;


    public ControllerCourierInfo(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void InitializeFragment(){
        Courier courier = (Courier) DataStorage.Get("CurrentCourier");

        TextView textViewLogin = view.findViewById(R.id.textViewLogin);
        TextView textViewName = view.findViewById(R.id.textViewName);
        TextView textViewLastName = view.findViewById(R.id.textViewLastName);

        textViewLogin.setText("Логин: "+courier.getLogin());
        textViewName.setText("Имя: "+courier.getName());
        textViewLastName.setText("Фамилия: "+courier.getLastName());
    }

    public void InitializeButtonsClick(){
        Button buttonShowOrders = view.findViewById(R.id.buttonShowOrders);
        buttonShowOrders.setOnClickListener(ButtonShowOrdersOnClick);
        Button buttonShowStats = view.findViewById(R.id.buttonShowStats);
        buttonShowStats.setOnClickListener(ButtonShowStats);
        ImageButton buttonTalkWithCourier = view.findViewById(R.id.imageButtonTalkWirhCourier);
        buttonTalkWithCourier.setOnClickListener(ButtonTalkWithCourier);
        Button buttonBack = view.findViewById(R.id.buttonBackToCourierList);
        buttonBack.setOnClickListener(ButtonBackOnClick);
    }

    private View.OnClickListener ButtonBackOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CouriersListFragment couriersListFragment = new CouriersListFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, couriersListFragment);
            fragmentTransaction.commit();
        }
    };

    private View.OnClickListener ButtonTalkWithCourier = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TalkWithCourierFragment talkWithCourierFragment = new TalkWithCourierFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, talkWithCourierFragment);
            fragmentTransaction.commit();
        }
    };

    private View.OnClickListener ButtonShowOrdersOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OrdersListFragment ordersListFragment = new OrdersListFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, ordersListFragment);
            fragmentTransaction.commit();
        }
    };

    private View.OnClickListener ButtonShowStats = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CourierStatsFragment courierStatsFragment = new CourierStatsFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, courierStatsFragment);
            fragmentTransaction.commit();
        }
    };
}
