package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.entities.Courier;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbManager;
import com.example.mymanagerapp.view.CouriersListFragment;
import com.example.mymanagerapp.view.OrdersListFragment;

public class ControllerAddNewOrder {
    private View view;
    private DbManager db;


    public ControllerAddNewOrder(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void InitializeButton(){
        Button buttonAddNewCourier = view.findViewById(R.id.buttonAddOrder);
        buttonAddNewCourier.setOnClickListener(ButtonAddNewOrderOnClick);
    }

    private View.OnClickListener ButtonAddNewOrderOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            EditText editTextAddress = view.findViewById(R.id.editTextNewOrderAddress);
            EditText editTextOrder = view.findViewById(R.id.editTextNewOrderInfo);


            String address = editTextAddress.getText().toString();
            String order = editTextOrder.getText().toString();
            int courierId = ((Courier) DataStorage.Get("CurrentCourier")).getId();


            db.getTableOrders().AddNew(address,order,courierId);

            OrdersListFragment ordersListFragment = new OrdersListFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, ordersListFragment);
            fragmentTransaction.commit();
        }
    };
}
