package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.entities.Order;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbManager;


import java.util.ArrayList;

public class RvAdapterOrdersList extends RecyclerView.Adapter<RvAdapterOrdersList.OrderViewHolder> {

    ArrayList<Order> orders;
    DbManager db;

    public RvAdapterOrdersList(ArrayList<Order> orders) {
        this.orders = orders;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewAddress;
        public TextView textViewOrder;
        public TextView textViewCourierName;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewAddress = itemView.findViewById(R.id.textViewAddress);
            textViewOrder = itemView.findViewById(R.id.textViewOrder);
            textViewCourierName = itemView.findViewById(R.id.textViewCourierName);

        }
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_item_order,viewGroup,false);
        OrderViewHolder ovh = new OrderViewHolder(v);
        return ovh;
    }



    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int i) {
        //holder.textViewCourierName.setText(db.getTableCouriers().GetById(orders.get(i).getUserId()).getName()+" "+db.getTableCouriers().GetById(orders.get(i).getId()).getLastName());
        holder.textViewAddress.setText(orders.get(i).getAddress());
        holder.textViewOrder.setText(orders.get(i).getOrder());
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }
}
