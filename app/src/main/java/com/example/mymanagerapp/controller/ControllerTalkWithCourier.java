package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbManager;
import com.example.mymanagerapp.view.CourierInfoFragment;

public class ControllerTalkWithCourier extends FragmentActivity {

    private View view;
    private DbManager db;
    boolean connectError = false;

    public ControllerTalkWithCourier(View view) {
        this.view = view;
        db = DbManager.GetInstance((Context) DataStorage.Get("context"));
    }

    public void InitializeButtons() {
        Button buttonBack = view.findViewById(R.id.buttonBackFromManager);
        Button buttonSendMessage = view.findViewById(R.id.buttonSend);

        buttonBack.setOnClickListener(buttonBackOnClick);
        buttonSendMessage.setOnClickListener(buttonSendMessageOnClick);
    }

    private View.OnClickListener buttonBackOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CourierInfoFragment courierInfoFragment = new CourierInfoFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, courierInfoFragment);
            fragmentTransaction.commit();
        }
    };

    private View.OnClickListener buttonSendMessageOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView textViewSetMessage = view.findViewById(R.id.textViewMessage);
            EditText editTextGetMessage = view.findViewById(R.id.editTextMesage);

            TextView textViewErrorMessage = view.findViewById(R.id.textViewErrorMessage);
            textViewErrorMessage.setVisibility(View.INVISIBLE);

            if (!connectError){
                textViewSetMessage.setText(editTextGetMessage.getText().toString());
                editTextGetMessage.setText("");
                connectError = true;
            }

            textViewErrorMessage.setVisibility(View.VISIBLE);
        }
    };
}
