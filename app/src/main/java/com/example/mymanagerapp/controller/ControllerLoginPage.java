package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.entities.Courier;

import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbManager;
import com.example.mymanagerapp.view.CourierInfoFragment;
import com.example.mymanagerapp.view.CouriersListFragment;

public class ControllerLoginPage {
    private View view;
    private DbManager db;


    public ControllerLoginPage(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void InitializeButtonsClick(){
        Button buttonLogin = view.findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(OnButtonLoginClickListener);
    }

    private void AuthorizeManager(){
        EditText editTextLogin = view.findViewById(R.id.editTextLogin);
        EditText editTextPassword = view.findViewById(R.id.editTextPassword);

        String login = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();

        Context context = (Context) DataStorage.Get("context");

        Courier courier = db.getTableCouriers().GetByLoginAndPassword(login,password);

        if(courier != null)
        {
            CouriersListFragment couriersListFragment = new CouriersListFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, couriersListFragment);
            fragmentTransaction.commit();
        }
    }

    private View.OnClickListener OnButtonLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AuthorizeManager();
        }
    };
}
