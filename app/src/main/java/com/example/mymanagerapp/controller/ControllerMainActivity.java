package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbManager;
import com.example.mymanagerapp.view.CourierInfoFragment;
import com.example.mymanagerapp.view.LoginPageFragment;

public class ControllerMainActivity {
    private MainActivity mainActivity;

    private DbManager db;

    private LoginPageFragment loginPageFragment;
    private CourierInfoFragment courierInfoFragment;

    public ControllerMainActivity(MainActivity mainActivity) {
        this.mainActivity =  mainActivity;
        DataStorage.Add("mainActivity", mainActivity);
        db = DbManager.GetInstance(this.mainActivity.getApplicationContext());
    }

    public void InitializeFragments(){
        loginPageFragment = new LoginPageFragment();
        courierInfoFragment = new CourierInfoFragment();
        DataStorage.Add("LastId","3");
        DataStorage.Add("LastOrderId","3");

        FragmentTransaction fragmentTransaction;
        fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.fragmentsContainerMain,loginPageFragment);

        fragmentTransaction.commit();
    }
}
