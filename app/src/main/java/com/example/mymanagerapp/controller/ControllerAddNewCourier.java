package com.example.mymanagerapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mymanagerapp.MainActivity;
import com.example.mymanagerapp.R;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbManager;
import com.example.mymanagerapp.view.CouriersListFragment;

public class ControllerAddNewCourier {
    private View view;
    private DbManager db;


    public ControllerAddNewCourier(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void InitializeButton(){
        Button buttonAddNewCourier = view.findViewById(R.id.buttonAddOrder);
        buttonAddNewCourier.setOnClickListener(ButtonAddNewCourierOnClick);
    }

    private View.OnClickListener ButtonAddNewCourierOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            EditText editTextName = view.findViewById(R.id.editTextNewCourierName);
            EditText editTextLastName = view.findViewById(R.id.editTextNewCourierLastName);
            EditText editTextLogin = view.findViewById(R.id.editTextNewCourierLogin);
            EditText editTextPassword = view.findViewById(R.id.editTextNewCourierPassword);

            String name = editTextName.getText().toString();
            String lastName = editTextLastName.getText().toString();
            String login = editTextLogin.getText().toString();
            String password  = editTextPassword.getText().toString();

            db.getTableCouriers().AddNew(name,lastName,login,password);

            CouriersListFragment couriersListFragment = new CouriersListFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, couriersListFragment);
            fragmentTransaction.commit();
        }
    };
}
