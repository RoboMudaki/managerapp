package com.example.mymanagerapp.tools;

import android.content.Context;

import com.example.mymanagerapp.Tables.TableCouriers;
import com.example.mymanagerapp.Tables.TableManagers;
import com.example.mymanagerapp.Tables.TableOrders;

public class DbManager {
    private static DbManager instance = null;

    public static DbManager GetInstance(Context context)
    {
        if (instance==null)
        {
            instance = new DbManager(context);
        }
        return instance;
    }

    private TableCouriers tableCouriers;
    private TableOrders tableOrders;
    private TableManagers tableManagers;

    private DbManager(Context context) {
        DbHelper dbHelper = new DbHelper(context);

        tableCouriers = new TableCouriers(dbHelper);
        tableOrders = new TableOrders(dbHelper);
        tableCouriers = new TableCouriers(dbHelper);
    }

    public TableCouriers getTableCouriers() {
        return tableCouriers;
    }

    public TableOrders getTableOrders() {
        return tableOrders;
    }

    public TableManagers getTableManager() {
        return tableManagers;
    }
}
