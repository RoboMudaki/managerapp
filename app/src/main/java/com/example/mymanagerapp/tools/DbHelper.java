package com.example.mymanagerapp.tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context)
    {
        super(context,"app7.db",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS \"couriers\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"login\"\tTEXT NOT NULL,\n" +
                "\t\"password\"\tTEXT NOT NULL,\n" +
                "\t\"name\"\tTEXT NOT NULL,\n" +
                "\t\"last_name\"\tTEXT NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS \"managers\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"login\"\tTEXT NOT NULL,\n" +
                "\t\"password\"\tTEXT NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS \"orders\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"addres\"\tTEXT NOT NULL,\n" +
                "\t\"orderInfo\"\tTEXT NOT NULL,\n" +
                "\t\"userId\"\tINTEGER NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ");");

        db.execSQL("INSERT INTO \"couriers\" (\"id\",\"login\",\"password\",\"name\",\"last_name\") VALUES (1,'vik','123','Алексей','Балалаев'),\n" +
                " (2,'shiki','342','Михаил','Кащенко'),\n" +
                " (3,'jopa','76378','Дмитрий','Свердыщев');");

        db.execSQL("INSERT INTO \"managers\" (\"id\",\"login\",\"password\") VALUES (1,'vik','123'),\n" +
                " (2,'shiki','342'),\n" +
                " (3,'jopa','76378');");

        db.execSQL("INSERT INTO \"orders\" (\"id\",\"addres\",\"orderInfo\",\"userId\") VALUES (1,'Костычева 52','Пицца реперони, 0.5л Кола',1),\n" +
                " (2,'Ульянова 27','Пицца четыре сыра',1),\n" +
                " (3,'Орловская 13','0.5 Фанта, Пицца барбекю',2);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
