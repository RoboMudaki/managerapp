package com.example.mymanagerapp.Tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mymanagerapp.entities.Order;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbHelper;

import java.util.ArrayList;

public class TableOrders {
    private DbHelper dbHelper;

    public TableOrders(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public ArrayList<Order> GetOrdersByUserId(int userId){
        ArrayList<Order> orders = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `orders` WHERE id = "+"'"+userId+"'";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        while (cursor.moveToNext() == true)
        {
            Order order = new Order(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getInt(3)
            );

            orders.add(order);
        }

        cursor.close();
        dbHelper.close();

        return orders;
    }


    public void AddNew(String address,String order,int userId){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int lastId = Integer.parseInt((String) DataStorage.Get("LastOrderId"))+1;

        String sqlCommand = "INSERT INTO `orders` (addres,orderInfo,userId) VALUES ('"+address+"','"+order+"','"+userId+"')" ;

        DataStorage.Add("LastOrderId",String.valueOf(lastId));

        db.execSQL(sqlCommand);

        dbHelper.close();
    }


    public void DeleteById(int id){
        SQLiteDatabase db  = dbHelper.getWritableDatabase();

        String sqlCommand = "DELETE FROM `orders` WHERE `id` = "+id;

        db.execSQL(sqlCommand,null);
    }
}
