package com.example.mymanagerapp.Tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mymanagerapp.entities.Courier;
import com.example.mymanagerapp.entities.Manager;
import com.example.mymanagerapp.tools.DbHelper;

import java.util.ArrayList;

public class TableManagers {
    private DbHelper dbHelper;

    public TableManagers(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }



    public Manager GetByLoginAndPassword(String login, String password)
    {
        Manager manager = null;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `managers` WHERE login='"+login+"' AND password='"+password+"'";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        while (cursor.moveToNext() == true)
        {
            manager = new Manager(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2)
            );
        }

        cursor.close();
        dbHelper.close();

        return manager;
    }

    public ArrayList<Manager> GetCouriers(){
        ArrayList<Manager> managers = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `managers`";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        while (cursor.moveToNext() == true)
        {
            Manager manager = new Manager(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2)
            );

            managers.add(manager);
        }

        cursor.close();
        dbHelper.close();

        return managers;
    }

    public boolean ExistCourierByLogin(String login)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `managers` WHERE login='"+login+"'";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        boolean exist = cursor.moveToNext();

        cursor.close();
        dbHelper.close();

        return exist;
    }

    public Manager GetById(int managerId)
    {
        Manager manager = null;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `manager` WHERE id="+managerId;

        Cursor cursor = db.rawQuery(sqlCommand,null);

        if(cursor.moveToNext()==true)
        {
            manager = new Manager(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2)
            );
        }

        cursor.close();
        dbHelper.close();

        return manager;
    }
}
