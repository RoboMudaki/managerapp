package com.example.mymanagerapp.Tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mymanagerapp.entities.Courier;
import com.example.mymanagerapp.entities.Order;
import com.example.mymanagerapp.tools.DataStorage;
import com.example.mymanagerapp.tools.DbHelper;

import java.util.ArrayList;

public class TableCouriers {
    private DbHelper dbHelper;

    public TableCouriers(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }



    public Courier GetByLoginAndPassword(String login, String password)
    {
        Courier courier = null;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `couriers` WHERE login='"+login+"' AND password='"+password+"'";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        while (cursor.moveToNext() == true)
        {
            courier = new Courier(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)
            );
        }

        cursor.close();
        dbHelper.close();

        return courier;
    }

    public ArrayList<Courier> GetCouriers(){
        ArrayList<Courier> couriers = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `couriers`";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        while (cursor.moveToNext() == true)
        {
            Courier courier = new Courier(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)
            );

            couriers.add(courier);
        }

        cursor.close();
        dbHelper.close();

        return couriers;
    }

    public void AddNew(String name,String lastName,String login,String password){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int lastId = Integer.parseInt((String) DataStorage.Get("LastId"))+1;

        String sqlCommand = "INSERT INTO `couriers` (id,login,password,name,last_name) VALUES ('"+lastId+"','"+login+"','"+password+"','"+name+"','"+lastName+"')" ;

        DataStorage.Add("LastId",String.valueOf(lastId));

        db.execSQL(sqlCommand);

        dbHelper.close();
    }

    public boolean ExistCourierByLogin(String login)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `couriers` WHERE login='"+login+"'";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        boolean exist = cursor.moveToNext();

        cursor.close();
        dbHelper.close();

        return exist;
    }

    public Courier GetById(int courierId)
    {
        Courier courier = null;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `couriers` WHERE id="+"'"+courierId+"'";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        if(cursor.moveToNext()==true)
        {
            courier = new Courier(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)
            );
        }

        cursor.close();
        dbHelper.close();

        return courier;
    }
}
